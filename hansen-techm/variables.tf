variable "aws_access_key" {
  default = ""
}

variable "aws_secret_key" {
  default = ""
}

variable "aws_region" {
  default = "ap-southeast-2"
}

variable "ami_id" {
  default = "ami-0cd1a46beaedb6f3e"
}

variable "sg_id" {
  default = ["sg-05819d2f59b8c5a6c"]
}

variable "ami_rid" {
  default = "ami-037b603c0fe50da63"
}

variable "vpc_id" {
  default = "vpc-0740ae81a86808410"
}

variable "subnet_id" {
  default = "subnet-01ef767d10a8c3c74"
}

variable "instance_type" {
  default = "t3.xlarge"
}

variable "dinstance_type" {
  default = "m5.large"
}

variable "pinstance_type" {
  default = "t2.micro"
}

variable "winstance_type" {
  default = "t2.large"
}

variable "sinstance_type" {
  default = "t3.2xlarge"
}

variable "key_name" {
  default = "hansen"
}

variable "key_sname" {
  default = "hansen"
}

variable "aws_account_id" {
  default = "143951677320"
}

variable "rootdevice" {
  type        = map(string)
  description = "rootdevice properties"
  default = {
    volume_type           = "gp2"
    volume_size           = 100
    delete_on_termination = "true"
  }
}

variable "srootdevice" {
  type        = map(string)
  description = "rootdevice properties"
  default = {
    volume_type           = "gp2"
    volume_size           = 100
    delete_on_termination = "true"
  }
}

