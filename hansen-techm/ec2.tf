module "Catalog" {
  source              = "../modules/ec2/"
  ami_id              = var.ami_id
  vpc_id              = var.vpc_id
  subnet_id           = var.subnet_id 
  security_group_ids  = var.sg_id
  ebs_optimized       = "true"
  key_name            = var.key_name
  privateip           = "10.7.28.196"
  instance_type       = var.instance_type
  instancename        = "Catalog"
  root_block_device   = var.rootdevice
}
/*
module "Catalog Service" {
  source              = "../modules/ec2/"
  ami_id              = var.ami_rid
  vpc_id              = var.vpc_id
  subnet_id           = var.subnet_id 
  security_group_ids  = var.sg_id
  ebs_optimized       = "true"
  key_name            = var.key_name
  privateip           = "10.7.28.197"
  instance_type       = var.instance_type
  instancename        = "Catalog Service"
  root_block_device   = var.rootdevice
}

module "CPQ" {
  source              = "../modules/ec2/"
  ami_id              = var.ami_rid
  vpc_id              = var.vpc_id
  subnet_id           = var.subnet_id 
  security_group_ids  = var.sg_id
  ebs_optimized       = "true"
  key_name            = var.key_name
  privateip           = "10.7.28.197"
  instance_type       = var.instance_type
  instancename        = "Catalog Service"
  root_block_device   = var.rootdevice
}


*/