
resource "aws_route53_record" "rancher_dns_record" {
  zone_id = var.zone_id
  name    = "${var.env}-rancher.techmbm.com"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.rancher-manager.public_ip]
}



module "elb" {
  source  = "terraform-aws-modules/elb/aws"
  version = "~> 2.0"
  name    = "${var.env}-${var.project}-techmahindra"

# Public Subnet required for External communication
  subnets         = ["subnet-04b8d686a177fe035"]
  security_groups = ["sg-0093cc974eaa5d134"]
  internal        = false

  listener = [
    {
      instance_port     = "32080"
      instance_protocol = "http"
      lb_port           = "80"
      lb_protocol       = "http"
    },
    {
      instance_port     = "32443"
      instance_protocol = "tcp"
      lb_port           = "2443"
      lb_protocol       = "tcp"
    },
    {
      instance_port      = "32080"
      instance_protocol  = "http"
      lb_port            = "443"
      lb_protocol        = "https"
      ssl_certificate_id = module.acm.this_acm_certificate_arn
    },
  ]

  health_check = {
    target              = "TCP:32080"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }
  tags = {
    Environment = var.env
    Name        = var.project
    Project     = var.project
    Role        = "ELB"
  }
  # // ELB attachments
  number_of_instances = 1
  instances           = flatten(data.aws_instances.worker.*.ids)
}

resource "aws_route53_record" "st_www" {
 depends_on = [time_sleep.wait_480_seconds]
  zone_id = var.zone_id
  name    = "${var.env}-nginx.${var.domain_name}"
  type    = "A"

  alias {
    name                   = module.elb.this_elb_dns_name
    zone_id                = module.elb.this_elb_zone_id
    evaluate_target_health = true
  }
}

resource "time_sleep" "wait_50_seconds" {
  depends_on = [aws_route53_record.rancher_dns_record]

  create_duration = "50s"
}

resource "aws_route53_record" "test_dns_record" {
  depends_on = [time_sleep.wait_480_seconds]
  count   = length(var.dns)
  zone_id = var.zone_id
  name    = element(var.dns, count.index)
  ttl     = "300"
  #records = ["${var.env}-nginx.${var.domain_name}"]
  records = ["${var.env}-nginx.${var.domain_name}"]
}

resource "local_file" "config" {
    depends_on = [time_sleep.wait_480_seconds]
    content     = rancher2_cluster.k8s.kube_config
    filename = "~/.kube/config"
  provisioner "local-exec" {
    command = "kubectl apply -f storageclass.yaml"
  }
}

output "kubeconfig" {
  value = rancher2_cluster.k8s.kube_config
}


module "acm" {
  source      = "../modules/acm"
  domain_name = var.domain_name
  zone_id     = var.zone_id
  subject_alternative_names = [
    "*.${var.domain_name}",
  ]
  wait_for_validation = true
  tags = {
    Environment = var.environment
    Name        = var.domain_name
    Project     = var.project
    Role        = "ACM"
  }
}
