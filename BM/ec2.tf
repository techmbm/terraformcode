
resource "aws_instance" "rancher-manager" {
  ami                         = var.ami_rancher
  instance_type               = "t2.medium"
  vpc_security_group_ids      = ["sg-0093cc974eaa5d134"]
  subnet_id                   = var.subnet_id
  key_name                    = var.key_pair_name
  associate_public_ip_address = true
  root_block_device {
    volume_type           = var.root_block_device["volume_type"]
    volume_size           = var.root_block_device["volume_size"]
    delete_on_termination = var.root_block_device["delete_on_termination"]
  }

  tags = {
    Name        = "${var.env}-rancher-manager"
    Environment = var.env
    Role        = "${var.env}-rancher-manager"
  }
  connection {
    type     = "ssh"
    user     = "ubuntu"
    private_key = file("~/devops.pem")
    port     = 22022
    host     = aws_instance.rancher-manager.private_ip
  }
  provisioner "remote-exec" {
    inline = [
        "sudo mkdir /rancher",
        "sudo rm /var/cache/apt/archives/lock",
        "sudo rm /var/lib/dpkg/lock",
        "sudo rm /var/lib/dpkg/lock-frontend",
        "sudo apt-get update",
        "sudo apt install docker.io -y",
        "sudo systemctl start docker",
        "sudo systemctl enable docker",
        "sudo docker run -d -v /rancher:/var/lib/rancher -p 80:80 -p 443:443 --restart=unless-stopped rancher/rancher:v2.3.5",
    ]
  }
}
