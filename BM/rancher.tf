
provider "rancher2" {
  alias = "bootstrap"
  api_url   = "https://${var.env}-rancher.techmbm.com/v3"
  bootstrap = true
  insecure = true
}

resource "rancher2_bootstrap" "admin" {
  provider = rancher2.bootstrap
  depends_on = [time_sleep.wait_50_seconds]
  password = var.password
  telemetry = true
}

provider "rancher2" {
  alias = "admin"

  api_url = rancher2_bootstrap.admin.url
  token_key = rancher2_bootstrap.admin.token
  insecure = true
}

resource "rancher2_cluster" "k8s" {
  provider = rancher2.admin
  name        = var.env
  description = "rancher2 Amazon EC2 cluster for aio"
  rke_config {
    cloud_provider {
      name = "aws"
    }
    kubernetes_version = "v1.16.8-rancher1-2"
    network {
      plugin = "canal"
    }
    ingress {
      provider = "none"
    }
  }  
}

resource "rancher2_node_template" "etcd-cp-template1" {
  provider = rancher2.admin
  name        = "etcd-cp-template1"
  description = var.env
  amazonec2_config {
    access_key           = var.aws_access_key
    secret_key           = var.aws_secret_key
    ami                  = var.ami_rancher
    instance_type        = "t2.medium"
    region               = var.aws_region
    security_group       = ["sg-0093cc974eaa5d134"]
    iam_instance_profile = "Terraform_Role"
    subnet_id            = "subnet-04b8d686a177fe035"
    vpc_id               = "vpc-0221f3c417c689267"
    zone                 = "a"
    root_size            = "30"
    private_address_only = true
  }
}

resource "rancher2_node_pool" "etcdplane" {
  provider = rancher2.admin
  cluster_id       = rancher2_cluster.k8s.id
  name             = "${var.env}-etcd-cp"
  hostname_prefix  = "${var.env}-etcd-cp-0"
  node_template_id = rancher2_node_template.etcd-cp-template1.id
  quantity         = 1  
  control_plane    = true
  etcd             = true
}

resource "rancher2_node_template" "worker-template1" {
  provider = rancher2.admin
  name        = "worker-template1"
  description = var.env
  amazonec2_config {
    access_key           = var.aws_access_key
    secret_key           = var.aws_secret_key
    ami                  = var.ami_rancher
    instance_type        = "t2.xlarge"
    region               = var.aws_region
    security_group       = ["sg-0093cc974eaa5d134"]
    iam_instance_profile = "Terraform_Role"
    subnet_id            = "subnet-04b8d686a177fe035"
    vpc_id               = "vpc-0221f3c417c689267"
    zone                 = "a"
    root_size            = "100"
    private_address_only = true
  }
}

resource "rancher2_node_pool" "worker" {
  provider = rancher2.admin
  cluster_id       = rancher2_cluster.k8s.id
  name             = "${var.env}-worker"
  hostname_prefix  = "${var.env}-worker-0"
  node_template_id = rancher2_node_template.worker-template1.id
  quantity         = 1
  worker           = true
}

resource "time_sleep" "wait_480_seconds" {
  depends_on = [rancher2_cluster.k8s]

  create_duration = "480s"
}

data "aws_instances" "worker" {
  depends_on = [time_sleep.wait_480_seconds]
  count = 1
  instance_tags = {
    Name   = "${var.env}-worker-${count.index}"
    "kubernetes.io/cluster/${rancher2_cluster.k8s.id}" = "owned"
  }
}

resource "rancher2_namespace" "dev-ingress" {
  depends_on = [local_file.config]
  provider = rancher2.admin
  name = "dev-ingress"
  project_id = rancher2_cluster.k8s.default_project_id
  description = "nginx namespace"
}

resource "rancher2_namespace" "bm" {
  depends_on = [local_file.config]
  provider = rancher2.admin
  name = "${var.env}-bm"
  project_id = rancher2_cluster.k8s.default_project_id
  description = "env namespace"
}

