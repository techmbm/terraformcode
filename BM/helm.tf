
provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

resource "helm_release" "ingress" {
  depends_on = [rancher2_namespace.dev-ingress]
  name       = "dev-nginx"
  namespace  = "dev-ingress"
  chart      = "stable/nginx-ingress"
  version    = "1.40.2"
  values = [
    "${file("../apps/ingress-nginx/values.yaml")}"
  ]
}

resource "null_resource" "axon" {
  depends_on = [helm_release.ingress]
  provisioner "local-exec" {
    command = "kubectl apply -f ../apps/axon/01-deployment.yaml"
  }
}

resource "null_resource" "mongo" {
  depends_on = [null_resource.axon]
  provisioner "local-exec" {
    command = "kubectl apply -f ../apps/mongo/deployment.yaml"
  }
}

resource "null_resource" "mongo-secret" {
  depends_on = [null_resource.mongo]
  provisioner "local-exec" {
    command = "kubectl apply -f ../apps/mongo-express/01-mongo-express.yaml"
  }
}

resource "helm_release" "mongo-express" {
  depends_on = [null_resource.mongo-secret]
  name       = "mongo-express"
  namespace  = "default"
  chart      = "../apps/mongo-express/charts"
  values = [
    "${file("../apps/mongo-express/values.yaml")}"
  ]
}

resource "helm_release" "prometheus" {
  depends_on = [helm_release.ingress]
  name       = "prometheus"
  namespace  = "kube-system"
  chart      = "prometheus-community/prometheus"
  values = [
    "${file("../apps/prometheus/values.yaml")}"
  ]
}

resource "helm_release" "elasticsearch" {
  depends_on = [helm_release.ingress]
  name       = "elasticsearch"
  namespace  = "kube-system"
  chart      = "elastic/elasticsearch"
  version    = "7.9.3"
  values = [
    "${file("../apps/elasticsearch/values.yaml")}"
  ]
}

resource "helm_release" "flunet-bit" {
  depends_on = [helm_release.ingress]
  name       = "flunet-bit"
  namespace  = "kube-system"
  chart      = "stable/fluent-bit"
  values = [
    "${file("../apps/fluent-bit/values.yaml")}"
  ]
}

resource "null_resource" "kibana-secret" {
  depends_on = [helm_release.ingress]
  provisioner "local-exec" {
    command = "kubectl apply -f ../apps/kibana/01-kibana-basic-auth-ingress.yaml"
  }
}

resource "helm_release" "kibana" {
  depends_on = [null_resource.kibana-secret]
  name       = "kibana"
  namespace  = "kube-system"
  chart      = "elastic/kibana"
  version    = "7.9.3"
  values = [
    "${file("../apps/kibana/values.yaml")}"
  ]
}

resource "null_resource" "grafana-secret" {
  depends_on = [helm_release.ingress]
  provisioner "local-exec" {
    command = "kubectl apply -f ../apps/grafana/grafana-secret.yaml"
  }
}

resource "helm_release" "grafana" {
  depends_on = [null_resource.grafana-secret]
  name       = "grafana"
  namespace  = "kube-system"
  chart      = "stable/grafana"
  values = [
    "${file("../apps/grafana/values.yaml")}"
  ]
}

