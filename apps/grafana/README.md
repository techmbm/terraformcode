## Kubernetes Setup for Grafana


Install

```
kubectl create -f grafana-secret.yaml
helm install --name grafana --namespace kube-system stable/grafana -f values.yaml
```

Access grafana on https://grafana.tmbmarble.com/

Get your 'admin' user password by running:

```
kubectl get secret --namespace kube-system monitoring-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```