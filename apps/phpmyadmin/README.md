# phpmyadmin


## Install

```
helm install --name phpmyadmin --namespace kube-system -f values.yaml stable/phpmyadmin
```

## Documentation

See the full documentation [here](https://github.com/helm/charts/tree/master/stable/phpmyadmin)