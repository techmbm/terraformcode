# Prometheus


## Install

```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install --name prometheus --namespace kube-system -f values.yaml stable/prometheus


helm install  prometheus --namespace kube-system -f environment/release/apps/prometheus/values.yaml stable/prometheus
```

## Documentation

See the full documentation [here](https://github.com/helm/charts/tree/master/stable/prometheus)
