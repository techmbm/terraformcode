# Flunet-bit

```bash
helm install -f environment/staging/apps/fluent-bit/values.yaml flunet-bit stable/fluent-bit
```

## source code

<https://github.com/ranjith-ka/fluent-bit>

## Helm chart

<https://github.com/ranjith-ka/fluentd-helm-charts>
