# Elasticsearch Helm3

## Add Repository

```bash
helm repo add elastic https://helm.elastic.co
```

## Install ES

```bash
helm upgrade -f test/apps/elasticsearch/values.yaml elasticsearch elastic/elasticsearch --version 7.9.3 -n kube-system
helm install -f test/apps/elasticsearch/data.yaml  elasticsearch-data elastic/elasticsearch --version 7.9.3 -n elasticsearch
```

<https://github.com/elastic/helm-charts/tree/master/elasticsearch>
