data "aws_ami" "base_ami" {
  most_recent = true

  filter {
    name = "name"
    values = [var.ami_pattern[var.ami]]
  }

  # owners = ["137112412989", "309956199498", "801119661308", "099720109477"]
  owners = ["099720109477", "331800868831"]
}

// EC2 Instance Resource for Module

resource "aws_instance" "ec2_instance" {
  ami                     = var.ami_id
  subnet_id               = var.subnet_id
  instance_type           = var.instance_type
  vpc_security_group_ids  = var.security_group_ids
  key_name                = var.key_name
  ebs_optimized           = var.ebs_optimized
  source_dest_check       = var.sdcheck
  private_ip              = var.privateip
  iam_instance_profile    = var.iamrole
  user_data               = var.user_data 
  disable_api_termination = var.Termination

  root_block_device {
    volume_type           = var.root_block_device["volume_type"]
    volume_size           = var.root_block_device["volume_size"]
    delete_on_termination = var.root_block_device["delete_on_termination"]
  }

  tags = {
    Name = var.instancename
    Billing = var.billing
    cost_center = var.cost_center
    application = var.application
  }

}

