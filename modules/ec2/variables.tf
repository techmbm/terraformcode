variable "ami" {
  default = "ubuntu"
}

variable "ami_pattern" {
  default = {
    "awsami"     = "Amazon Linux AMI*"
    "linux"      = "RHEL-7.5_HVM_GA-*"
    "windows"    = "Windows_Server-2016-English-Full-Base*"
    "ubuntu"     = "ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server*"
    "grafana"    = "ag_ami_packer_custom_grafana*"
    "nginx"      = "ag_ami_packer_custom_nginx*"
    "pgadmin"    = "ag_ami_pgadmin*"
    "buildbotm"  = "ag_ami_buildbot_master*"
    "buildbotlw" = "ag_ami_buildbot_linux_worker*"
    "ubuntu18"   = "ag_ami_ubuntu_18_04*"
    "pypi"       = "ag_ami_pypi_cloud*"
    "ansible"    = "ag_ami_packer_custom_ansible"
    "Rstudio"    = "ag_ami_rstudio_server_pro"
    "rbknginx"   = "ag_ami_nginx_analytics_backend"
  }
}

variable "key_name" {
  default = ""
}

variable "billing" {
  default = "BlueMarble"
}

variable "Termination" {
  default = "false"
}

variable "iamrole" {
  default = ""
}

variable "subnet_id" {
  default = ""
}

variable "ebs_optimized" {
  default = "false"
}

variable "security_group_ids" {
  default = ["sg-0093cc974eaa5d134"]
}

variable "sdcheck" {
  default = "true"
}

variable "cost_center" {
  default = "BM"
}

variable "application" {
  default = "Blue Marble"
}

variable "ami_type" {
  default = "noncustom"
}

variable "ami_id" {
  default = "ami-016343a5b7a603046"
}

variable "privateip" {
  default = ""
}

variable "instance_type" {
  default = "t2.micro"
}

variable "vpc_id" {
  default     = ""
  description = "The VPC subnet the instance(s) will go in"
}

variable "subnet_type" {
  default     = "public"
  description = "Subnet type. Either private or public."
}

variable "subnet_index" {
  description = "Subnet index - in case multiple subnets found and you want to use concrete one."
  default     = 1
}

variable "iam_role" {
  default = "base"
}

variable "root_block_device" {
  default = {
    volume_type           = "gp2"
    volume_size           = 30
    delete_on_termination = true
  }
}


variable "extra_tags" {
  type        = map(string)
  description = "Any number of custom tags"
  default     = {}
}

variable "user_data" {
  default = ""
  type    = string
}

variable "private_ip" {
  default = ""
}

variable "availability_zones" {
  default     = ["ap-south-1a", "ap-south-1b"]
  description = "The availability zones the we should create subnets in, launch instances in, and configure for ELB and ASG"
}

variable "instancename" {
  default = ""
}

variable "availability_zones_tag" {
  default     = ["ap-south-1a", "ap-south-1b"]
  description = "The availability zones the we should create subnets in, launch instances in, and configure for ELB and ASG"
}

