variable "aws_access_key" {
  default = ""
}

variable "aws_secret_key" {
  default = ""
}

variable "aws_region" {
  default = "ap-south-1"
}

variable "ami_id" {
  default = "ami-016343a5b7a603046"
}

variable "ami_wid" {
  default = "ami-0878eaff79e053a2f"
}

variable "ami_did" {
  default = "ami-00f0d9699a1c567a1"
}

variable "vpc_id" {
  default = "vpc-0221f3c417c689267"
}

variable "subnet_id" {
  default = "subnet-04b8d686a177fe035"
}

variable "instance_type" {
  default = "t2.medium"
}

variable "dinstance_type" {
  default = "m5.large"
}

variable "pinstance_type" {
  default = "t2.micro"
}

variable "winstance_type" {
  default = "t2.large"
}

variable "hinstance_type" {
  default = "t2.xlarge"
}

variable "key_name" {
  default = "devops"
}

variable "key_sname" {
  default = "siebel"
}

variable "aws_account_id" {
  default = "143951677320"
}

variable "rootdevice" {
  type        = map(string)
  description = "rootdevice properties"
  default = {
    volume_type           = "gp2"
    volume_size           = 30
    delete_on_termination = "true"
  }
}

variable "srootdevice" {
  type        = map(string)
  description = "rootdevice properties"
  default = {
    volume_type           = "gp2"
    volume_size           = 100
    delete_on_termination = "true"
  }
}

