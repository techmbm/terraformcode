
module "docker" {
  source              = "../modules/ec2/"
  ami_id              = var.ami_id
  vpc_id              = var.vpc_id
  subnet_id           = var.subnet_id 
  ebs_optimized       = "true"
  key_name            = var.key_name
  privateip           = "10.7.23.85"
  instance_type       = var.dinstance_type
  instancename        = "Bluemarble Docker"
  root_block_device   = var.rootdevice
}


module "jenkins" {
  source              = "../modules/ec2/"
  ami_id              = var.ami_id
  vpc_id              = var.vpc_id
  subnet_id           = var.subnet_id
  key_name            = var.key_name
  privateip           = "10.7.23.90"
  instance_type       = var.instance_type
  instancename        = "Bluemarble jenkins"
  root_block_device   = var.rootdevice
}

module "BPM" {
  source              = "../modules/ec2/"
  ami_id              = var.ami_wid
  vpc_id              = var.vpc_id
  subnet_id           = var.subnet_id
  key_name            = var.key_name
  privateip           = "10.7.23.81"
  instance_type       = var.winstance_type
  instancename        = "BPM_Windows"
  billing             = "BPM"
  application         = "BPM"
  cost_center         = "BPM"
  root_block_device   = var.rootdevice
}

module "kubernetes" {
  source              = "../modules/ec2/"
  ami_id              = var.ami_id
  vpc_id              = var.vpc_id
  subnet_id           = var.subnet_id
  key_name            = var.key_name
  privateip           = "10.7.23.70"
  instance_type       = var.instance_type
  instancename        = "Rancher"
  billing             = "kubernetes"
  application         = "kubernetes"
  cost_center         = "kubernetes"
  root_block_device   = var.rootdevice
}

module "DTAG" {
  source              = "../modules/ec2/"
  ami_id              = var.ami_id
  vpc_id              = var.vpc_id
  subnet_id           = var.subnet_id
  key_name            = var.key_name
  privateip           = "10.7.23.74"
  instance_type       = var.instance_type
  instancename        = "DTAG"
  billing             = "DTAG"
  application         = "DTAG"
  cost_center         = "DTAG"
  root_block_device   = var.srootdevice
}

module "WSO2" {
  source              = "../modules/ec2/"
  ami_id              = var.ami_id
  vpc_id              = var.vpc_id
  subnet_id           = var.subnet_id
  key_name            = var.key_name
  privateip           = "10.7.23.75"
  instance_type       = var.instance_type
  instancename        = "WSO2"
  billing             = "WSO2"
  application         = "WSO2"
  cost_center         = "WSO2"
  root_block_device   = var.srootdevice
}

module "HansenPOC2" {
  source              = "../modules/ec2/"
  ami_id              = var.ami_wid
  vpc_id              = var.vpc_id
  subnet_id           = var.subnet_id
  key_name            = var.key_name
  privateip           = "10.7.23.71"
  instance_type       = var.hinstance_type
  instancename        = "HansenPOC2"
  billing             = "HansenPOC2"
  application         = "HansenPOC2"
  cost_center         = "HansenPOC2"
  root_block_device   = var.srootdevice
}

module "HansenPOC3" {
  source              = "../modules/ec2/"
  ami_id              = var.ami_wid
  vpc_id              = var.vpc_id
  subnet_id           = var.subnet_id
  key_name            = var.key_name
  privateip           = "10.7.23.72"
  instance_type       = var.hinstance_type
  instancename        = "HansenPOC3"
  billing             = "HansenPOC3"
  application         = "HansenPOC3"
  cost_center         = "HansenPOC3"
  root_block_device   = var.srootdevice
}

module "HansenPOC4" {
  source              = "../modules/ec2/"
  ami_id              = var.ami_wid
  vpc_id              = var.vpc_id
  subnet_id           = var.subnet_id
  key_name            = var.key_name
  privateip           = "10.7.23.73"
  instance_type       = var.hinstance_type
  instancename        = "HansenPOC4"
  billing             = "HansenPOC4"
  application         = "HansenPOC4"
  cost_center         = "HansenPOC4"
  root_block_device   = var.srootdevice
}



/*
module "siebel_web" {
  source              = "../modules/ec2/"
  ami_id              = var.ami_wid
  vpc_id              = var.vpc_id
  subnet_id           = var.subnet_id
  key_name            = var.key_sname
  privateip           = "10.7.23.70"
  instance_type       = var.sinstance_type
  instancename        = "siebel crm web"
  root_block_device   = var.srootdevice
}

module "siebel_db" {
  source              = "../modules/ec2/"
  ami_id              = var.ami_wid
  vpc_id              = var.vpc_id
  subnet_id           = var.subnet_id
  key_name            = var.key_sname
  privateip           = "10.7.23.71"
  instance_type       = var.sinstance_type
  instancename        = "siebel crm db"
  root_block_device   = var.srootdevice

}



resource "aws_volume_attachment" "crmweb" {
  
  device_name = "/dev/sda2"
  volume_id   = aws_ebs_volume.example.id
  instance_id = module.siebel_web.ec2_id
}
*/
resource "aws_ebs_volume" "example" {
  availability_zone = "ap-south-1a"
  size              = 100

  tags = {
    Name = "CRMWEB"
  }
}