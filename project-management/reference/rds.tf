module "rds" {
  source = "../modules/rds/"

  vpc_sgroup              = module.rdssecuritygroup.sg_id
  rds_instance_identifier = "eng-rds-pg96"
  rds_engine_type         = var.rds_engine_type
  rds_engine_version      = var.rds_engine_version
  rds_instance_class      = var.rds_instance_class
  rds_allocated_storage   = var.rds_allocated_storage
  license_model           = "postgresql-license"

  #cwatch_log             = ["alert", "audit", "error", "general", "listener", "slowquery", "trace"]
  database_name           = "eng_prod"
  database_user           = var.dbuser
  database_password       = var.dbpass
  database_port           = var.dbport
  backup_window           = var.dbbackupwindow
  backup_retention_period = var.dbreten
  rds_is_multi_az         = var.rds_multi_az
  Name                    = "eng-rds-pg96"

  #ssm_param              = "param--dbpwd"
  parameter_group_name = "rdsparams"

  #kms_key_name           = "${var.kmsname}"
  subnet_ids             = "rds-subnet"
  db_parameter_group     = "postgres9.6"
  ssm_parameter_required = "no"
}

resource "aws_db_subnet_group" "RDS-subnet" {
  name = "rds-subnet"

  #subnet_ids = ["${aws_subnet.database.id}","${aws_subnet.SOGETI_CPP_SUBNET_DB_1B.id}"]
  #subnet_ids = ["${aws_subnet.database.*.id}"]
  #subnet_ids = [element(module.vpc_main.public_subnet_ids, 1), element(module.vpc_main.public_subnet_ids, 2)]
   subnet_ids = concat(element(module.vpc_main.public_subnet_ids, 1), element(module.vpc_main.public_subnet_ids, 2))

  tags = {
    Name = "RDSSUBNETGROUP"
  }
}

