module "proxysec" {
  sgname = "analytics-sg-proxy"
  source = "../modules/secgroup/"
  vpc_id = module.vpc_analytics.vpc_id
}

resource "aws_security_group_rule" "allow_ingress_proxy" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.proxysec.sg_id
}

resource "aws_security_group_rule" "allow_egress_proxy" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.proxysec.sg_id
}

resource "aws_security_group_rule" "allow_ingress_proxyhttps" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.proxysec.sg_id
}

resource "aws_security_group_rule" "allow_ingress_proxyhttp" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.proxysec.sg_id
}

resource "aws_security_group_rule" "allow_ingress_proxynfs" {
  type              = "ingress"
  from_port         = 2049
  to_port           = 2049
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.proxysec.sg_id
}

module "analyticssec" {
  sgname = "analytics-sg-service"
  source = "../modules/secgroup/"
  vpc_id = module.vpc_analytics.vpc_id
}

resource "aws_security_group_rule" "allow_ingressanalytics" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "tcp"
  security_group_id        = module.analyticssec.sg_id
  source_security_group_id = module.proxysec.sg_id
}

resource "aws_security_group_rule" "allow_ingressanalyticsnfs" {
  type                     = "ingress"
  from_port                = 2049
  to_port                  = 2049
  protocol                 = "tcp"
  security_group_id        = module.analyticssec.sg_id
  source_security_group_id = module.rstudiosec.sg_id
}

module "analyticgrafana" {
  sgname = "analytics-sg-grafana"
  source = "../modules/secgroup/"
  vpc_id = module.vpc_analytics.vpc_id
}

resource "aws_security_group_rule" "allow_ingress_grafana" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "tcp"
  security_group_id        = module.analyticgrafana.sg_id
  source_security_group_id = module.proxysec.sg_id
}

module "rstudiosec" {
  sgname = "analytics-sg-rstudio"
  source = "../modules/secgroup/"
  vpc_id = module.vpc_analytics.vpc_id
}

resource "aws_security_group_rule" "allow_ingressrstudio" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "tcp"
  security_group_id        = module.rstudiosec.sg_id
  source_security_group_id = module.proxysec.sg_id
}

resource "aws_security_group_rule" "allow_ingressstudioself" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "tcp"
  security_group_id        = module.rstudiosec.sg_id
  source_security_group_id = module.rstudiosec.sg_id
}

resource "aws_security_group_rule" "allow_egressrstudio" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.rstudiosec.sg_id
}

