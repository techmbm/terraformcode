module "securitygroupnginx" {
  sgname = "sg-nginx"
  source = "../modules/secgroup/"
  vpc_id = module.vpc_main.vpc_id
}

module "securitygroupgrafana" {
  sgname = "sg-grafana"
  source = "../modules/secgroup/"
  vpc_id = module.vpc_main.vpc_id
}

module "securitygrouppgadmin" {
  sgname = "sg-pgadmin"
  source = "../modules/secgroup/"
  vpc_id = module.vpc_main.vpc_id
}

module "rdssecuritygroup" {
  sgname = "sg-rds"
  source = "../modules/secgroup/"
  vpc_id = module.vpc_main.vpc_id
}

module "bbotmsecuritygroup" {
  sgname = "sg-buildbotm"
  source = "../modules/secgroup/"
  vpc_id = module.vpc_main.vpc_id
}

resource "aws_security_group_rule" "allow_ingress" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.securitygroupnginx.sg_id
}

resource "aws_security_group_rule" "allow_egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.securitygroupnginx.sg_id
}

resource "aws_security_group_rule" "allow_ingress1" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.securitygroupnginx.sg_id
}

resource "aws_security_group_rule" "allow_ingress2" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.securitygroupnginx.sg_id
}

resource "aws_security_group_rule" "allow_egress2" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.securitygroupgrafana.sg_id
}

resource "aws_security_group_rule" "allow_ingressgrafana" {
  type      = "ingress"
  from_port = 0
  to_port   = 65535
  protocol  = "tcp"

  # cidr_blocks     =["${module.securitygroupnginx.sg_id}"]
  security_group_id        = module.securitygroupgrafana.sg_id
  source_security_group_id = module.securitygroupnginx.sg_id
}

resource "aws_security_group_rule" "allow_ingresspgadmin" {
  type                     = "ingress"
  from_port                = 5050
  to_port                  = 5050
  protocol                 = "tcp"
  security_group_id        = module.securitygrouppgadmin.sg_id
  source_security_group_id = module.securitygroupnginx.sg_id
}

module "graphitesecuritygroup" {
  sgname = "sg-graphite"
  source = "../modules/secgroup/"
  vpc_id = module.vpc_main.vpc_id
}

/*
module "k8sec" {
  sgname      = "sg-k8"
  source      = "../modules/secgroup/"
  vpc_id      = "${module.vpc_main.vpc_id}"
}
*/

resource "aws_security_group_rule" "allow_ingressbbotsame" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "tcp"
  security_group_id        = module.bbotmsecuritygroup.sg_id
  source_security_group_id = module.bbotmsecuritygroup.sg_id
}

resource "aws_security_group_rule" "allow_ingressgraphitenginx" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "tcp"
  security_group_id        = module.graphitesecuritygroup.sg_id
  source_security_group_id = module.securitygroupnginx.sg_id
}

/*
resource "aws_security_group_rule" "allow_ingressgraphiterstudio" {
  type            = "ingress"
  from_port       = 0
  to_port         = 65535
  protocol        = "tcp"
  security_group_id = "${module.graphitesecuritygroup.sg_id}"
  source_security_group_id = "${module.rstudiosec.sg_id}"

}*/

resource "aws_security_group_rule" "allow_egressgraphite" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.graphitesecuritygroup.sg_id
}

/*
resource "aws_security_group_rule" "allow_egressbk8" {
  type            = "egress"
  from_port       = 0
  to_port         = 0
  protocol        = "-1"
  cidr_blocks     =["0.0.0.0/0"]
  security_group_id = "${module.k8sec.sg_id}"

}

resource "aws_security_group_rule" "allow_ingressk8" {
  type            = "ingress"
  from_port       = 0
  to_port         = 65535
  protocol        = "tcp"
  security_group_id = "${module.k8sec.sg_id}"
  source_security_group_id = "${module.securitygroupnginx.sg_id}"

}*/

resource "aws_security_group_rule" "allow_ingressbbot" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "tcp"
  security_group_id        = module.bbotmsecuritygroup.sg_id
  source_security_group_id = module.securitygroupnginx.sg_id
}

resource "aws_security_group_rule" "allow_egresspgadmin" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.securitygrouppgadmin.sg_id
}

resource "aws_security_group_rule" "allow_egressbbot" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.bbotmsecuritygroup.sg_id
}

