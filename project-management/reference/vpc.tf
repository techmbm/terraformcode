module "vpc_main" {
  source              = "../modules/vpc/"
  vpc_cidr            = var.vpc_cidr
  public_subnet_cidr  = var.public_subnet_cidr
  private_subnet_cidr = var.private_subnet_cidr
  vpcname             = var.vpcname
  #  pubsubname             = "${var.pubsubname}"
  #prisubname             = ["${var.prisubname}"]
}

module "vpc_analytics" {
  source              = "../modules/analyticsvpc/"
  vpc_cidr            = var.avpc_cidr
  public_subnet_cidr  = var.apublic_subnet_cidr
  private_subnet_cidr = var.aprivate_subnet_cidr
  vpcname             = var.avpcname
  #  pubsubname             = "${var.pubsubname}"
  #prisubname             = ["${var.prisubname}"]
}

