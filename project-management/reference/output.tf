output "module-vpc_id" {
  value = module.vpc_main.vpc_id
}

output "nginx-public_ip" {
  value = module.nginx.public_ip
}

output "nginx-private_ip" {
  value = module.nginx.private_ip
}

output "grafana-public_ip" {
  value = module.grafana.private_ip
}

